import { BaseLanguageClient } from 'vscode-languageclient';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { CompletionStream } from './completion_stream';
import { createStreamIterator } from './create_stream_iterator';

jest.mock('./create_stream_iterator', () => ({
  createStreamIterator: jest.fn(),
}));

const mockCleanupFn = jest.fn();
const uniqueTrackingId = '1';

describe('CompletionStream', () => {
  it('binds the CompletionStream instance to the cleanup function ', () => {
    const stream = new CompletionStream({
      client: createFakePartial<BaseLanguageClient>({}),
      streamId: 'stream-1',
      uniqueTrackingId,
      onCancelDetached: mockCleanupFn,
    });

    const cleanupFn = (createStreamIterator as jest.Mock).mock.calls[0][3];
    cleanupFn();

    expect(mockCleanupFn).toHaveBeenCalledWith(stream);
  });
});
